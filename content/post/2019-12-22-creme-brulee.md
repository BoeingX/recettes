---
title: Crème brulée
tags: ["dessert"]
date: 2019-12-22
---

Cette recette est adaptée de [Hervé Cuisine](https://www.hervecuisine.com/recette/creme-brulee-a-la-vanille/).

## Ingrédients (pour 4 crème brulée de 65g)

- 20 cl de crème liquide entière
- 50 g de sucre glace
- 3 jaune d'oeufs
- 0,5 c.à café d'extrait de vanille

## Recettes

1. Faire chauffer la crème liquide à feu doux avec l'extrait de vanille
2. Blanchir les jaunes d'oeufs avec le sucre
3. Verser la crème tiédie et filtrée sur le mélange jaune d'oeufs et sucre
4. Continuer à mélanger sans incorporer beaucoup de bulle d'air dans l'appareil
5. Faire préchauffer le four à 150 degrés
6. Disposer la crème dans les ramequins et mettre à cuire au bain marie 30 à 35 minutes
7. A la sortie du four, mettre les crèmes brulée au frigo
8. Sapoudrer une couche de cassonade sur la crème et faire caraméliser à l'aide d'un chalumeau
