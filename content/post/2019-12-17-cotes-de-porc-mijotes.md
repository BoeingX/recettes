---
title: Côtes de porc mijotés
subtitle: ""
tags: ["plat", "chinois", "porc"]
date: 2019-12-17
---

## Ingrédients

- 3 côtes de porc
- 1 oignon
- 6 càs de sauce de soja
- 2 càc de sucre glace
- 1 gousse d'ail
- 1 clou de girofle
- 1 càc de gingembre moulu
- 1 càc de paprika (ou piment de cayenne)
- du sel et du poivre

## Recette

1. Mettre les côtes dans l'eau froide et alumer le feu
2. Laisser les côtes blanchir pendant 5 minutes environ
3. Sortir les côtes et les détailler en petit dés (de largeur 2cm environ)
4. Faire chauffer un filet d'huile (d'olive) dans une cocotte et faire revenir les oigons coupés
5. Ajouter les côtes et tous les autres ingrédients
6. Couvrir la cocotte et laisser le tout mijoter pendant au moins 1 heure
